<?php
/**
 * IUseThis API module
 */
class ApiIUT extends ApiBase {

	/**
	 * @var vote Instance of the IUT class, set in execute() below
	 */
	private $vote;

	/**
	 * Main entry point.
	 */
	public function execute() {
		$user = $this->getUser();

		// Get the request parameters
		$params = $this->extractRequestParams();

		$action = $params['what'];

		// If the "what" param isn't present, we don't know what to do!
		if ( !$action || $action === null ) {
			$this->dieUsageMsg( 'missingparam' );
		}

		// Need to have sufficient user rights to proceed...
		if ( !$user->isAllowed( 'iusethis' ) ) {
			$this->dieUsageMsg( 'badaccess-group0' );
		}

		// Ensure that the page ID is present and that it really is numeric
		$pageId = $params['pageId'];

		if ( !$pageId || $pageId === null || !is_numeric( $pageId ) ) {
			$this->dieUsageMsg( array( 'missingparam', 'pageId' ) );
		}

		$this->vote = new IUT( $pageId );

		switch ( $action ) {
			case 'delete':
				$this->vote->delete();
				$output = $this->vote->count( 1 );
				break;
			case 'vote':
			case 'iusethis':
			default:
				$this->vote->insert();
				$output = $this->vote->count( 1 );
				break;
		}

		// Top level
		$this->getResult()->addValue( null, $this->getModuleName(),
			array( 'result' => $output )
		);

		return true;
	}

	public function needsToken() {
		return 'csrf';
	}

	public function isWriteMode() {
		return true;
	}

	/**
	 * @return array
	 */
	public function getAllowedParams() {
		return array(
			'what' => array(
				ApiBase::PARAM_TYPE => 'string',
				ApiBase::PARAM_REQUIRED => true
			),
			'pageId' => array(
				ApiBase::PARAM_TYPE => 'integer',
				ApiBase::PARAM_REQUIRED => true
			),
			'type' => array(
				ApiBase::PARAM_TYPE => 'string',
			)
		);
	}

	/**
	 * @see ApiBase::getExamplesMessages()
	 */
	protected function getExamplesMessages() {
		return array(
			'action=voteny&what=vote&pageId=666' => 'apihelp-voteny-example-1',
			'action=voteny&what=delete&pageId=666' => 'apihelp-voteny-example-2',
			'action=voteny&what=vote&type=stars&pageId=666' => 'apihelp-voteny-example-3',
			'action=voteny&what=delete&type=stars&pageId=666' => 'apihelp-voteny-example-4',
			'action=voteny&what=multi&type=stars&pageId=666' => 'apihelp-voteny-example-5'
		);
	}
}
