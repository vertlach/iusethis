/**
 * JavaScript functions for Vote extension.
 *
 * TODO: Should refactor this into a jQuery widget.
 * The widget should get a PageID in its constructor so it can work on any page
 * for any page and with multiple instances per page.
 *
 * @constructor
 *
 * @author Jack Phoenix <jack@countervandalism.net>
 * @author Daniel A. R. Werner < daniel.a.r.werner@gmail.com >
 */
var IUT = function IUT() {
  this.clearRatingTimer = null;
  this.voted_new = [];
  this.id = 0;
  this.last_id = 0;
  this.imagePath = mw.config.get( 'wgExtensionAssetsPath' ) + '/VoteNY/images/';
  $.when( mw.loader.using( [ 'mediawiki.api.messages', 'mediawiki.jqueryMsg' ] ), $.ready ).then( function() {
    return new mw.Api().loadMessagesIfMissing( [
	'iusethis-unvote-link',
	'iusethis-link',
	'people-count'
    ] );
  })

  /**
   * Called when voting through the green square voting box
   *
   * @param TheVote
   * @param PageID Integer: internal ID number of the current article
   */
  this.clickVote = function( TheVote, PageID ) {
    ( new mw.Api() ).postWithToken( 'edit', {
      action: 'iusethis',
      format: 'json',
      what: 'vote',
      pageId: PageID
    } ).done( function( data ) {
      $( '#iutcount' ).html( data.iusethis.result + ' ' + mw.msg('people-count') );
      $( '#iutaction' ).html(
	  ' <a href="javascript:void(0);" class="vote-unvote-link">' +
	  mw.msg( 'iusethis-unvote-link' ) + '</a>'
	  );
    } );
  };

  /**
   * Called when removing your vote through the green square voting box
   *
   * @param PageID Integer: internal ID number of the current article
   */
  this.unVote = function( PageID ) {
    ( new mw.Api() ).postWithToken( 'edit', {
      action: 'iusethis',
      format: 'json',
      what: 'delete',
      pageId: PageID
    } ).done( function( data ) {
      $( '#iutcount' ).html( data.iusethis.result + ' ' + mw.msg('people-count') );
      $( '#iutaction' ).html(
	  ' <a href="javascript:void(0);" class="vote-vote-link">' +
	  mw.msg( 'iusethis-link' ) + '</a>'
	  );
    } );
  };

  this.startClearRating = function( id, rating, voted ) {
    var voteNY = this;
    this.clearRatingTimer = setTimeout( function() {
      voteNY.clearRating( id, 0, rating, voted );
    }, 200 );
  };

  this.clearRating = function( id, num, prev_rating, voted ) {
    if ( this.voted_new[id] ) {
      voted = this.voted_new[id];
    }
  };

  this.updateRating = function( id, num, prev_rating ) {
    if ( this.clearRatingTimer && this.last_id == id ) {
      clearTimeout( this.clearRatingTimer );
    }
    this.clearRating( id, num, prev_rating );
    this.last_id = id;
  };
};

// TODO: Make event handlers part of a widget as described in the VoteNY's TODO and reduce this
//       code to instantiating such a widget for the current wiki page if required.
$( function() {
  var vote = new IUT();

  // Green voting box's link
  $( '.iut-action' ).on( 'click', '> a', function( event ) {
    if ( $( this ).hasClass( 'vote-unvote-link' ) ) {
      vote.unVote( mw.config.get( 'wgArticleId' ) );
    } else {
      vote.clickVote( 1, mw.config.get( 'wgArticleId' ) );
    }
  } );

} );
