<?php
/**
 * All hooked functions used by IUseThis extension.
 *
 * @file
 * @ingroup Extensions
 */
class IUTHooks {

	/**
	 * Set up the <vote> parser hook.
	 *
	 * @param Parser $parser
	 * @return bool
	 */
	public static function registerParserHook( &$parser ) {
		$parser->setHook( 'iusethis', array( 'IUTHooks', 'renderVote' ) );
		return true;
	}

	/**
	 * Callback function for registerParserHook.
	 *
	 * @param string $input User-supplied input, unused
	 * @param array $args User-supplied arguments
	 * @param Parser $parser Instance of Parser, unused
	 * @return string HTML
	 */
	public static function renderVote( $input, $args, $parser ) {
		global $wgOut, $wgUser;

		// Disable parser cache (sadly we have to do this, because the caching is
		// messing stuff up; we want to show an up-to-date rating instead of old
		// or totally wrong rating, i.e. another page's rating...)
		$parser->disableCache();

		// Add CSS & JS
		// In order for us to do this *here* instead of having to do this in
		// registerParserHook(), we must've disabled parser cache
		$parser->getOutput()->addModuleStyles( 'ext.IUseThis.styles' );
		if ( $wgUser->isAllowed( 'iusethis' ) ) {
			$parser->getOutput()->addModules( 'ext.IUseThis.scripts' );
		}

		$output = null;
		$title = $wgOut->getTitle();
		if ( $title ) {
			$articleID = $title->getArticleID();
			$vote = new IUT( $articleID );

			$output = $vote->display();
		}

		return $output;
	}

	/**
	 * For the Renameuser extension.
	 *
	 * @param RenameuserSQL $renameUserSQL
	 * @return bool
	 */
	public static function onUserRename( $renameUserSQL ) {
		$renameUserSQL->tables['IUseThis'] = array( 'username', 'vote_user_id' );
		return true;
	}

	/**
	 * Main function to get the number of votes for a specific page
	 *
	 * @param Title $title Page to get votes for
	 * @return int Number of votes for the given page
	 */
	public static function getNumberOfVotesPage( Title $title ) {
		global $wgMemc;

		$id = $title->getArticleID();

		$key = wfMemcKey( 'iusethis', 'magic-word-page', $id );
		$data = $wgMemc->get( $key );

		if ( $data ) {
			return $data;
		} else {
			$dbr = wfGetDB( DB_SLAVE );

			$voteCount = (int)$dbr->selectField(
				'IUseThis',
				'COUNT(*) AS count',
				array( 'vote_page_id' => $id ),
				__METHOD__
			);

			$wgMemc->set( $key, $voteCount, 3600 );

			return $voteCount;
		}
	}

	/**
	 * Creates the necessary database table when the user runs
	 * maintenance/update.php.
	 *
	 * @param DatabaseUpdater $updater
	 * @return bool
	 */
	public static function addTable( $updater ) {
		$dbt = $updater->getDB()->getType();
		$file = __DIR__ . "/iusethis.$dbt";
		if ( file_exists( $file ) ) {
			$updater->addExtensionUpdate( array( 'addTable', 'IUseThis', $file, true ) );
		} else {
			throw new MWException( "IUseThis does not support $dbt." );
		}
		return true;
	}
}
