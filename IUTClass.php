<?php
/**
 * Vote class - class for handling vote-related functions (counting
 * the average score of a given page, inserting/updating/removing a vote etc.)
 *
 * @file
 * @ingroup Extensions
 */
class IUT {
	public $PageID = 0;
	public $Userid = 0;
	public $Username = null;

	/**
	 * Constructor
	 *
	 * @param int $pageID Article ID number
	 */
	public function __construct( $pageID ) {
		global $wgUser;

		$this->PageID = $pageID;
		$this->Username = $wgUser->getName();
		$this->Userid = $wgUser->getID();
	}

	/**
	 * Counts all votes, fetching the data from memcached if available
	 * or from the database if memcached isn't available
	 *
	 * @return int Amount of votes
	 */
	function count() {
		global $wgMemc;

		$key = wfMemcKey( 'iusethis', 'count', $this->PageID );
		$data = $wgMemc->get( $key );

		// Try cache
		if ( $data ) {
			wfDebug( "Loading vote count for page {$this->PageID} from cache\n" );
			$vote_count = $data;
		} else {
			$dbr = wfGetDB( DB_SLAVE );
			$vote_count = 0;
			$res = $dbr->select(
				'IUseThis',
				'COUNT(*) AS votecount',
				array( 'vote_page_id' => $this->PageID ),
				__METHOD__
			);
			$row = $dbr->fetchObject( $res );
			if ( $row ) {
				$vote_count = $row->votecount;
			}
			$wgMemc->set( $key, $vote_count );
		}

		return $vote_count;
	}

	/**
	 * Clear caches - memcached, parser cache and Squid cache
	 */
	function clearCache() {
		global $wgUser, $wgMemc;

		// Kill internal cache
		$wgMemc->delete( wfMemcKey( 'iusethis', 'count', $this->PageID ) );
		$wgMemc->delete( wfMemcKey( 'iusethis', 'avg', $this->PageID ) );

		// Purge squid
		$pageTitle = Title::newFromID( $this->PageID );
		if ( is_object( $pageTitle ) ) {
			$pageTitle->invalidateCache();
			$pageTitle->purgeSquid();

			// Kill parser cache
			$article = new Article( $pageTitle, /* oldid */0 );
			$parserCache = ParserCache::singleton();
			$parserKey = $parserCache->getKey( $article, $wgUser );
			$wgMemc->delete( $parserKey );
		}
	}

	/**
	 * Delete the user's vote from the database, purges normal caches.
	 */
	function delete() {
		$dbw = wfGetDB( DB_MASTER );
		$dbw->delete(
			'IUseThis',
			array(
				'vote_page_id' => $this->PageID,
				'username' => $this->Username
			),
			__METHOD__
		);

		$this->clearCache();
	}

	/**
	 * Inserts a new vote into the IUseThis database table
	 */
	function insert() {
		global $wgRequest;
		$dbw = wfGetDB( DB_MASTER );
		wfSuppressWarnings(); // E_STRICT whining
		$voteDate = date( 'Y-m-d H:i:s' );
		wfRestoreWarnings();
		if ( $this->UserAlreadyVoted() == false ) {
			$dbw->insert(
				'IUseThis',
				array(
					'username' => $this->Username,
					'vote_user_id' => $this->Userid,
					'vote_page_id' => $this->PageID,
					'vote_date' => $voteDate,
					'vote_ip' => $wgRequest->getIP(),
				),
				__METHOD__
			);

			$this->clearCache();
		}
	}

	/**
	 * Checks if a user has already voted
	 *
	 * @return bool|int False if s/he hasn't, otherwise returns the
	 *                  value of 'vote_value' column from Vote DB table
	 */
	function UserAlreadyVoted() {
		$dbr = wfGetDB( DB_SLAVE );
		$s = $dbr->selectRow(
			'IUseThis',
			'COUNT(*) as count',
			array(
				'vote_page_id' => $this->PageID,
				'username' => $this->Username
			),
			__METHOD__
		);
		if ( $s->count === "0" ) {
			return false;
		}
		return true;
	}

	/**
	 * Displays the green voting box
	 *
	 * @return string HTML output
	 */
	function display() {
		global $wgUser;

		$voted = $this->UserAlreadyVoted();

		$make_vote_box_clickable = '';
		if ( $voted === false ) {
			$make_vote_box_clickable = ' vote-clickable';
		}

		$output = "<div id='iutbox' class=\"noprint iut-box{$make_vote_box_clickable}\">";
	 	$output .= '<span id="iutcount" class="count">' . $this->count() . ' ' . wfMessage('people-count')->parse() . ' </span>';
		$output .= '<span id="iutaction" class="iut-action">';

		if ( !$wgUser->isAllowed( 'iusethis' ) ) {
			// @todo FIXME: this is horrible. If we don't have enough
			// permissions to vote, we should tell the end-user /that/,
			// not require them to log in!
			$login = SpecialPage::getTitleFor( 'Userlogin' );
			$output .= '<a class="votebutton" href="' .
				htmlspecialchars( $login->getFullURL() ) . '" rel="nofollow">' .
				wfMessage('iusethis-link')->parse() . '</a>';
		} else {
			if ( !wfReadOnly() ) {
				if ( $voted === false ) {
					$output .= '<a href="javascript:void(0);" class="iut-vote-link">' .
						wfMessage('iusethis-link')->parse() . '</a>';
				} else {
					$output .= '<a href="javascript:void(0);" class="iut-unvote-link">' .
						wfMessage('iusethis-unvote-link')->parse() . '</a>';
				}
			}
		}
		$output .= '</span>';
		$output .= '</div>';

		return $output;
	}
}
